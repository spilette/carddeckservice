package ca.pilette.logmein.carddeckservice.dealer.model;

import ca.pilette.logmein.carddeckservice.model.Card;
import ca.pilette.logmein.carddeckservice.model.CardPlayer;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DealerCardPlayer implements Comparable<DealerCardPlayer> {

  @Getter
  private final CardPlayer cardPlayer;
  @Getter
  private final Deque<Card> cards;

  public DealerCardPlayer(CardPlayer CardPlayer) {
    this.cardPlayer = CardPlayer;
    cards = new LinkedList<>();
  }

  public int getScore() {
    int result = 0;
    for (Card card : cards) {
      result += card.getRank().getFaceValue();
    }
    return result;
  }

  void getNewCards(List<Card> newCards) {
    cards.addAll(newCards);
  }

  void reset() {
    cards.clear();
  }

  @Override
  public String toString() {
    return "{ " +
      "cardPlayer={ " +
      cardPlayer.toString() +
      " }" +
      ", cards=[ " +
      String.join(",", cards.stream().map(o -> o.toString()).collect(Collectors.toList())) +
      " ]" +
      " }";
  }

  @Override
  public int compareTo(@NotNull DealerCardPlayer o) {
    if (this == o)
      return 0;
    return this.getScore() - o.getScore();
  }
}
