package ca.pilette.logmein.carddeckservice.dealer.model;

import ca.pilette.logmein.carddeckservice.dealer.exception.DuplicateNameException;
import ca.pilette.logmein.carddeckservice.dealer.exception.NotFoundException;
import ca.pilette.logmein.carddeckservice.model.Card;
import ca.pilette.logmein.carddeckservice.model.CardGame;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class DealerCardGame {

  @Getter
  private final CardGame cardGame;

  @Getter
  private final Set<DealerCardDeck> dealerCardDecks = new HashSet<>();

  @Getter
  private final Map<String, DealerCardPlayer> dealerCardPlayers = new HashMap<>();

  @Getter
  private final Deque<Card> cards;

  public DealerCardGame(CardGame cardGame) {
    this.cardGame = cardGame;
    cards = new LinkedList<>();
  }

  public void addCardDeck(DealerCardDeck cardDeck) {
    dealerCardDecks.add(cardDeck);
    shuffle();
  }

  public void addCardPlayer(DealerCardPlayer dealerCardPlayer) throws DuplicateNameException {
    String key = dealerCardPlayer.getCardPlayer().getName();
    if (dealerCardPlayers.containsKey(key)) {
      throw new DuplicateNameException("Player name already used in this game");
    }
    dealerCardPlayers.put(key, dealerCardPlayer);
    shuffle();
  }

  public DealerCardPlayer deleteCardPlayer(String name) {
    DealerCardPlayer player = dealerCardPlayers.remove(name);
    if (player != null) {
      shuffle();
    }
    return player;
  }

  public void shuffle() {
    clear();
    int capacity = 52 * dealerCardDecks.size();
    log.debug("shuffle() - will shuffle " + capacity + " cards");
    ArrayList<Card> newCardDeck = new ArrayList<>(capacity);
    for (DealerCardDeck deck : dealerCardDecks) {
      newCardDeck.addAll(Arrays.asList(Card.values()));
    }
    log.debug("shuffle() - newCardDeck.size()=" + newCardDeck.size());
    Random random = new SecureRandom();
    for (int i = 0; i < capacity; i++) {
      int index = random.nextInt(capacity);
      Card swap = newCardDeck.get(i);
      newCardDeck.set(i, newCardDeck.get(index));
      newCardDeck.set(index, swap);
    }
    cards.addAll(newCardDeck);
  }

  public void dealCardsToPlayer(String name, int count) throws NotFoundException {
    DealerCardPlayer player = dealerCardPlayers.get(name);
    if (player == null) {
      throw new NotFoundException("Player not found in this game");
    }
    ArrayList<Card> newCards = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      if (cards.isEmpty())
        break;
      newCards.add(cards.pop());
    }
    player.getNewCards(newCards);
  }

  public void clear() {
    for (DealerCardPlayer cardPlayer : dealerCardPlayers.values()) {
      cardPlayer.reset();
    }
    cards.clear();
    log.debug("clear() - cards.size()=" + cards.size());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("{");
    sb.append(" cardGame={");
    sb.append(" name=" + cardGame.getName());
    sb.append(" }, ");
    sb.append(" dealerCardDecks=[ ");
    sb.append(String.join(",", dealerCardDecks.stream().map(o -> o.toString()).collect(Collectors.toList())));
    sb.append(" ], ");
    sb.append(" dealerCardPlayers=[ ");
    sb.append(String.join(",", dealerCardPlayers.values().stream().map(o -> o.toString()).collect(Collectors.toList())));
    sb.append(" ], ");
    sb.append(" cards=[ ");
    sb.append(String.join(",", cards.stream().map(o -> o.toString()).collect(Collectors.toList())));
    sb.append(" ]");
    sb.append(" }");
    return sb.toString();
  }
}
