package ca.pilette.logmein.carddeckservice.dealer.model;

import ca.pilette.logmein.carddeckservice.model.CardDeck;
import lombok.Getter;

public class DealerCardDeck {

  @Getter
  private final CardDeck cardDeck;

  public DealerCardDeck(CardDeck cardDeck) {
    this.cardDeck = cardDeck;
  }
}
