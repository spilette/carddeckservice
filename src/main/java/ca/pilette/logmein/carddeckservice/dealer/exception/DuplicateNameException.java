package ca.pilette.logmein.carddeckservice.dealer.exception;

public class DuplicateNameException extends InvalidStateException {

  public DuplicateNameException() {
    super();
  }

  public DuplicateNameException(String message) {
    super(message);
  }

  public DuplicateNameException(String message, Throwable cause) {
    super(message, cause);
  }

  public DuplicateNameException(Throwable cause) {
    super(cause);
  }

  public DuplicateNameException(String message, Throwable cause,
                                boolean enableSuppression,
                                boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
