package ca.pilette.logmein.carddeckservice.dealer;

import ca.pilette.logmein.carddeckservice.dealer.exception.DuplicateNameException;
import ca.pilette.logmein.carddeckservice.dealer.exception.InvalidStateException;
import ca.pilette.logmein.carddeckservice.dealer.exception.NotFoundException;
import ca.pilette.logmein.carddeckservice.dealer.model.DealerCardGame;
import ca.pilette.logmein.carddeckservice.model.*;

public abstract class CardDealer {

  public abstract void init();

  public abstract void createNewCardGame(CardGame cardGame) throws DuplicateNameException;

  public abstract void deleteCardGame(String cardGameName) throws NotFoundException;

  public abstract void createNewCardDeck(CardDeck cardDeck);

  public abstract int getAvailableCardDeckCount();

  public abstract void addCardDeckToGame(String cardGameName) throws InvalidStateException;

  public abstract void addNewCardPlayer(String cardGameName, CardPlayer cardPlayer) throws NotFoundException, DuplicateNameException;

  public abstract void deleteCardPlayer(String cardGameName, String cardPlayerId) throws NotFoundException;

  public abstract DealerCardGame getDealerCardGame(String cardGameName) throws NotFoundException;

  public abstract void shuffleCardGameDeck(String cardGameName) throws NotFoundException;

  public abstract void dealCards(String cardGameName, String playerName, int count) throws NotFoundException;

  public abstract CardsList getCardsForPlayer(String cardGameName, String playerName) throws NotFoundException;

  public abstract PlayersList getCardGamePlayersList(String cardGameName) throws NotFoundException;

  public abstract CardsDistribution getCardGameDeckDistribution(String cardGameName) throws NotFoundException;

  public abstract CardsList getRemainingCardsInGame(String cardGameName) throws NotFoundException;

}
