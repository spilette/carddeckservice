package ca.pilette.logmein.carddeckservice.dealer.provider;

import ca.pilette.logmein.carddeckservice.dealer.CardDealer;
import ca.pilette.logmein.carddeckservice.dealer.exception.DuplicateNameException;
import ca.pilette.logmein.carddeckservice.dealer.exception.InvalidStateException;
import ca.pilette.logmein.carddeckservice.dealer.exception.NotFoundException;
import ca.pilette.logmein.carddeckservice.dealer.model.DealerCardDeck;
import ca.pilette.logmein.carddeckservice.dealer.model.DealerCardGame;
import ca.pilette.logmein.carddeckservice.dealer.model.DealerCardPlayer;
import ca.pilette.logmein.carddeckservice.model.*;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.*;

@Slf4j
@Singleton
public class CardDealerInMemory extends CardDealer {

  private Map<String, DealerCardGame> cardGameMap = new HashMap<>();
  private Deque<DealerCardDeck> availableCardDeckStack = new ArrayDeque<>();

  public static CardDealerInMemory getInstance() {
    return SingletonHolder.getInstance();
  }

  @Override
  public void init() {
    log.debug("CardDealerInMemory.init() was called");
  }

  @Override
  public void createNewCardGame(CardGame cardGame) throws DuplicateNameException {
    if (cardGameMap.containsKey(cardGame.getName())) {
      throw new DuplicateNameException("A CardGame with that name already exists: " + cardGame.getName());
    }
    cardGameMap.put(cardGame.getName(), new DealerCardGame(cardGame));
  }

  @Override
  public DealerCardGame getDealerCardGame(String cardGameName) throws NotFoundException {
    return doGetDealerCardGame(cardGameName);
  }

  @Override
  public void deleteCardGame(String cardGameName) throws NotFoundException {
    DealerCardGame dealerCardGame = doGetDealerCardGame(cardGameName);
    dealerCardGame.clear();
    cardGameMap.remove(cardGameName);
  }

  @Override
  public void addNewCardPlayer(String cardGameName, CardPlayer cardPlayer) throws NotFoundException, DuplicateNameException {
    DealerCardGame dealerCardGame = doGetDealerCardGame(cardGameName);
    dealerCardGame.addCardPlayer(new DealerCardPlayer(cardPlayer));
  }

  @Override
  public void deleteCardPlayer(String cardGameName, String cardPlayerName) throws NotFoundException {
    DealerCardGame dealerCardGame = doGetDealerCardGame(cardGameName);
    DealerCardPlayer player = dealerCardGame.deleteCardPlayer(cardPlayerName);
    if (player == null) {
      throw new NotFoundException("Could not find Card Player with id: " + cardPlayerName + " in game with name: " + cardGameName);
    }
  }

  @Override
  public void createNewCardDeck(CardDeck cardDeck) {
    DealerCardDeck dealerCardDeck = new DealerCardDeck(cardDeck);
    availableCardDeckStack.push(dealerCardDeck);
  }

  @Override
  public int getAvailableCardDeckCount() {
    return availableCardDeckStack.size();
  }

  @Override
  public void addCardDeckToGame(String cardGameName) throws InvalidStateException {
    DealerCardGame dealerCardGame = doGetDealerCardGame(cardGameName);
    try {
      dealerCardGame.addCardDeck(availableCardDeckStack.pop());
    }
    catch (NoSuchElementException e) {
      throw new InvalidStateException("No CardDeck available.");
    }
  }

  @Override
  public void shuffleCardGameDeck(String cardGameName) throws NotFoundException {
    DealerCardGame dealerCardGame = doGetDealerCardGame(cardGameName);
    dealerCardGame.shuffle();
  }

  @Override
  public void dealCards(String cardGameName, String playerName, int count) throws NotFoundException {
    DealerCardGame dealerCardGame = doGetDealerCardGame(cardGameName);
    dealerCardGame.dealCardsToPlayer(playerName, count);
  }

  @Override
  public CardsList getCardsForPlayer(String cardGameName, String playerName) throws NotFoundException {
    CardsList result = new CardsList();
    DealerCardPlayer player = doGetDealerCardPlayer(cardGameName, playerName);
    result.setCards(Collections.unmodifiableList(new ArrayList(player.getCards())));
    return result;
  }

  @Override
  public PlayersList getCardGamePlayersList(String cardGameName) throws NotFoundException {
    DealerCardGame dealerCardGame = doGetDealerCardGame(cardGameName);
    List<DealerCardPlayer> result = new ArrayList<>(dealerCardGame.getDealerCardPlayers().values());
    result.sort(Comparator.reverseOrder());
    return new PlayersList(result);
  }

  @Override
  public CardsDistribution getCardGameDeckDistribution(String cardGameName) throws NotFoundException {

    DealerCardGame dealerCardGame = doGetDealerCardGame(cardGameName);
    CardsDistribution cardsDistribution = new CardsDistribution();
    for (Card card : dealerCardGame.getCards()) {
      cardsDistribution.increment(card.getSuit());
    }
    return cardsDistribution;
  }

  public CardsList getRemainingCardsInGame(String cardGameName) throws NotFoundException {
    CardsList result = new CardsList();
    DealerCardGame cardGame = doGetDealerCardGame(cardGameName);
    List<Card> cards = new ArrayList<>(cardGame.getCards());
    cards.sort(Comparator.naturalOrder());
    result.setCards(Collections.unmodifiableList(new ArrayList(cards)));
    return result;
  }

  private DealerCardGame doGetDealerCardGame(String cardGameName) throws NotFoundException {
    if (cardGameName == null || cardGameName.isEmpty()) {
      throw new NotFoundException("Could not find CardGame without a name.");
    }
    DealerCardGame dealerCardGame = cardGameMap.get(cardGameName);
    if (dealerCardGame == null) {
      throw new NotFoundException("Could not find CardGame with name: " + cardGameName);
    }
    return dealerCardGame;
  }

  private DealerCardPlayer doGetDealerCardPlayer(String cardGameName, String playerName) throws NotFoundException {
    DealerCardGame dealerCardGame = doGetDealerCardGame(cardGameName);
    DealerCardPlayer result = dealerCardGame.getDealerCardPlayers().get(playerName);
    if (result == null) {
      throw new NotFoundException("Could not find CardPlayer with name: " + playerName);
    }
    return result;
  }

  private static final class SingletonHolder {

    private static CardDealerInMemory SINGLETON;

    private synchronized static void init() {
      if (SINGLETON == null) {
        SINGLETON = new CardDealerInMemory();
      }
    }

    static CardDealerInMemory getInstance() {
      if (SINGLETON == null) {
        init();
      }
      return SINGLETON;
    }
  }
}
