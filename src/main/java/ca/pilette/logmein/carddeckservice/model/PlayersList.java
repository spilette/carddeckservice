package ca.pilette.logmein.carddeckservice.model;

import ca.pilette.logmein.carddeckservice.dealer.model.DealerCardPlayer;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class PlayersList {

  @NotEmpty
  @Getter
  @Setter
  List<DealerCardPlayer> players;

  public PlayersList() {

  }

  public PlayersList(List<DealerCardPlayer> players) {
    this.players = players;
  }

  @Override
  public String toString() {
    return players.toString();
  }
}
