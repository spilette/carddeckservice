package ca.pilette.logmein.carddeckservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.NotEmpty;

public enum Card implements Comparable<Card> {

  KING_OF_HEARTS(Suit.HEARTS, Rank.KING),
  QUEEN_OF_HEARTS(Suit.HEARTS, Rank.QUEEN),
  JACK_OF_HEARTS(Suit.HEARTS, Rank.JACK),
  TEN_OF_HEARTS(Suit.HEARTS, Rank.TEN),
  NINE_OF_HEARTS(Suit.HEARTS, Rank.NINE),
  EIGHT_OF_HEARTS(Suit.HEARTS, Rank.EIGHT),
  SEVEN_OF_HEARTS(Suit.HEARTS, Rank.SEVEN),
  SIX_OF_HEARTS(Suit.HEARTS, Rank.SIX),
  FIVE_OF_HEARTS(Suit.HEARTS, Rank.FIVE),
  FOUR_OF_HEARTS(Suit.HEARTS, Rank.FOUR),
  THREE_OF_HEARTS(Suit.HEARTS, Rank.THREE),
  TWO_OF_HEARTS(Suit.HEARTS, Rank.TWO),
  ACE_OF_HEARTS(Suit.HEARTS, Rank.ACE),

  KING_OF_SPADES(Suit.SPADES, Rank.KING),
  QUEEN_OF_SPADES(Suit.SPADES, Rank.QUEEN),
  JACK_OF_SPADES(Suit.SPADES, Rank.JACK),
  TEN_OF_SPADES(Suit.SPADES, Rank.TEN),
  NINE_OF_SPADES(Suit.SPADES, Rank.NINE),
  EIGHT_OF_SPADES(Suit.SPADES, Rank.EIGHT),
  SEVEN_OF_SPADES(Suit.SPADES, Rank.SEVEN),
  SIX_OF_SPADES(Suit.SPADES, Rank.SIX),
  FIVE_OF_SPADES(Suit.SPADES, Rank.FIVE),
  FOUR_OF_SPADES(Suit.SPADES, Rank.FOUR),
  THREE_OF_SPADES(Suit.SPADES, Rank.THREE),
  TWO_OF_SPADES(Suit.SPADES, Rank.TWO),
  ACE_OF_SPADES(Suit.SPADES, Rank.ACE),

  KING_OF_CLUBS(Suit.CLUBS, Rank.KING),
  QUEEN_OF_CLUBS(Suit.CLUBS, Rank.QUEEN),
  JACK_OF_CLUBS(Suit.CLUBS, Rank.JACK),
  TEN_OF_CLUBS(Suit.CLUBS, Rank.TEN),
  NINE_OF_CLUBS(Suit.CLUBS, Rank.NINE),
  EIGHT_OF_CLUBS(Suit.CLUBS, Rank.EIGHT),
  SEVEN_OF_CLUBS(Suit.CLUBS, Rank.SEVEN),
  SIX_OF_CLUBS(Suit.CLUBS, Rank.SIX),
  FIVE_OF_CLUBS(Suit.CLUBS, Rank.FIVE),
  FOUR_OF_CLUBS(Suit.CLUBS, Rank.FOUR),
  THREE_OF_CLUBS(Suit.CLUBS, Rank.THREE),
  TWO_OF_CLUBS(Suit.CLUBS, Rank.TWO),
  ACE_OF_CLUBS(Suit.CLUBS, Rank.ACE),

  KING_OF_DIAMONDS(Suit.DIAMONDS, Rank.KING),
  QUEEN_OF_DIAMONDS(Suit.DIAMONDS, Rank.QUEEN),
  JACK_OF_DIAMONDS(Suit.DIAMONDS, Rank.JACK),
  TEN_OF_DIAMONDS(Suit.DIAMONDS, Rank.TEN),
  NINE_OF_DIAMONDS(Suit.DIAMONDS, Rank.NINE),
  EIGHT_OF_DIAMONDS(Suit.DIAMONDS, Rank.EIGHT),
  SEVEN_OF_DIAMONDS(Suit.DIAMONDS, Rank.SEVEN),
  SIX_OF_DIAMONDS(Suit.DIAMONDS, Rank.SIX),
  FIVE_OF_DIAMONDS(Suit.DIAMONDS, Rank.FIVE),
  FOUR_OF_DIAMONDS(Suit.DIAMONDS, Rank.FOUR),
  THREE_OF_DIAMONDS(Suit.DIAMONDS, Rank.THREE),
  TWO_OF_DIAMONDS(Suit.DIAMONDS, Rank.TWO),
  ACE_OF_DIAMONDS(Suit.DIAMONDS, Rank.ACE);

  @NotEmpty
  @JsonProperty
  @Getter
  private final Suit suit;

  @NotEmpty
  @JsonProperty
  @Getter
  private final Rank rank;

  Card(Suit suit, Rank rank) {
    this.suit = suit;
    this.rank = rank;
  }

  public static enum Suit {
    HEARTS,
    SPADES,
    CLUBS,
    DIAMONDS,
  }

  public static enum Rank {
    ACE("Ace", 1),
    TWO("2", 2),
    THREE("3", 3),
    FOUR("4", 4),
    FIVE("5", 5),
    SIX("6", 6),
    SEVEN("7", 7),
    EIGHT("8", 8),
    NINE("9", 9),
    TEN("10", 10),
    JACK("JACK", 11),
    QUEEN("QUEEN", 12),
    KING("KING", 13);

    @NotEmpty
    @JsonProperty
    @Getter
    private final String name;

    @NotEmpty
    @JsonProperty
    @Getter
    private final int faceValue;

    Rank(String name, int faceValue) {
      this.name = name;
      this.faceValue = faceValue;
    }
  }
}
