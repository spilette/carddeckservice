package ca.pilette.logmein.carddeckservice.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class CardsList {

  @NotEmpty
  @Getter
  @Setter
  List<Card> cards;

  @Override
  public String toString() {
    return cards.toString();
  }
}
