package ca.pilette.logmein.carddeckservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

public class CardDeal {

  @NotEmpty
  @JsonProperty
  @Getter
  @Setter
  private int count;

}
