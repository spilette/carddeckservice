package ca.pilette.logmein.carddeckservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

public class CardsDistribution {


  @NotEmpty
  @JsonProperty
  @Getter
  @Setter
  int heartsCount;

  @NotEmpty
  @Getter
  @Setter
  int spadesCount;

  @NotEmpty
  @JsonProperty
  @Getter
  @Setter
  int clubsCount;

  @NotEmpty
  @JsonProperty
  @Getter
  @Setter
  int diamondsCount;

  @JsonIgnore
  public void increment(Card.Suit suit) {
    switch (suit) {
      case HEARTS:
        this.heartsCount++;
        break;
      case SPADES:
        this.spadesCount++;
        break;
      case CLUBS:
        this.clubsCount++;
        break;
      case DIAMONDS:
        this.diamondsCount++;
        break;
    }
  }

  @Override
  public String toString() {
    return "[ " + heartsCount + " hearts, " + spadesCount + " spades, " + clubsCount + " clubs, " + diamondsCount + " diamonds ]";
  }
}
