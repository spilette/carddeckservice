package ca.pilette.logmein.carddeckservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

public class CardDeck {

  @JsonProperty
  @Getter
  @Setter
  private UUID id;

}
