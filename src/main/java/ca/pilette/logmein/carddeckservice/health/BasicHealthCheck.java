package ca.pilette.logmein.carddeckservice.health;

import com.codahale.metrics.health.HealthCheck;

public class BasicHealthCheck extends HealthCheck {

  public static final String getName() {
    return "BasicHealthCheck";
  }

  @Override
  protected Result check() throws Exception {
    return HealthCheck.Result.healthy();
  }
}
