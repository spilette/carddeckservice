package ca.pilette.logmein.carddeckservice.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

public class CardDeckServiceConfiguration extends Configuration {

  @NotEmpty
  @JsonProperty
  @Getter
  @Setter
  private String houseName;

  @NotEmpty
  @JsonProperty
  @Getter
  @Setter
  private String publicEndpoint;
}
