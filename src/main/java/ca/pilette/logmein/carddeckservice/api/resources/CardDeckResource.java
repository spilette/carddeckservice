package ca.pilette.logmein.carddeckservice.api.resources;

import ca.pilette.logmein.carddeckservice.dealer.CardDealer;
import ca.pilette.logmein.carddeckservice.model.CardDeck;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/cardDecks")
@Slf4j
public class CardDeckResource {

  private final CardDealer cardDealer;

  public CardDeckResource(final CardDealer cardDealer) {
    this.cardDealer = cardDealer;
  }

  @POST
  @Produces({"application/json"})
  public Response createCardDeck(CardDeck cardDeck,
                                 @Context UriInfo uriInfo,
                                 @Context HttpHeaders httpHeaders) {

    cardDealer.createNewCardDeck(cardDeck);
    return Response.noContent().build();
  }

}

