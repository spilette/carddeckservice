package ca.pilette.logmein.carddeckservice.api.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

public class ApiErrorMessage {

  @NotEmpty
  @JsonProperty
  @Getter
  @Setter
  private String message;

  public ApiErrorMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "{ message=" + message + " }";
  }
}
