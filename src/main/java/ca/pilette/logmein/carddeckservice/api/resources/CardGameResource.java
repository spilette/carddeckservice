package ca.pilette.logmein.carddeckservice.api.resources;

import ca.pilette.logmein.carddeckservice.api.error.ApiErrorMessage;
import ca.pilette.logmein.carddeckservice.config.CardDeckServiceConfiguration;
import ca.pilette.logmein.carddeckservice.dealer.CardDealer;
import ca.pilette.logmein.carddeckservice.dealer.exception.DuplicateNameException;
import ca.pilette.logmein.carddeckservice.dealer.exception.InvalidStateException;
import ca.pilette.logmein.carddeckservice.dealer.exception.NotFoundException;
import ca.pilette.logmein.carddeckservice.model.*;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;

@Path("/cardGames")
@Slf4j
public class CardGameResource {

  private final CardDeckServiceConfiguration configuration;
  private final CardDealer cardDealer;

  public CardGameResource(final CardDeckServiceConfiguration configuration, final CardDealer cardDealer) {
    this.configuration = configuration;
    this.cardDealer = cardDealer;
  }

  @POST
  @Produces({"application/json"})
  public Response createCardGame(CardGame cardGame,
                                 @Context UriInfo uriInfo,
                                 @Context HttpHeaders httpHeaders) {

    try {
      cardDealer.createNewCardGame(cardGame);
      UriBuilder builder = uriInfo.getAbsolutePathBuilder().path(cardGame.getName());
      return Response.created(URI.create(configuration.getPublicEndpoint() + builder.build().getPath())).build();
    }
    catch (DuplicateNameException e) {
      return Response.status(Response.Status.CONFLICT)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }

  }


  @DELETE
  @Produces({"application/json"})
  @Path("/{cardGameId}")
  public Response deleteCardGame(@PathParam("cardGameId") String cardGameName,
                                 @Context HttpHeaders httpHeaders) {
    try {
      cardDealer.deleteCardGame(cardGameName);
      return Response.noContent().build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }

  @POST
  @Produces({"application/json"})
  @Path("/{cardGameId}/cardDecks")
  public Response addCardDeck(@PathParam("cardGameId") String cardGameName,
                              @Context UriInfo uriInfo,
                              @Context HttpHeaders httpHeaders) {

    try {
      cardDealer.addCardDeckToGame(cardGameName);
      return Response.noContent().build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
    catch (InvalidStateException e) {
      return Response.status(Response.Status.BAD_REQUEST)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }

  @POST
  @Produces({"application/json"})
  @Path("/{cardGameId}/cardPlayers")
  public Response addCardPlayer(@PathParam("cardGameId") String cardGameName,
                                CardPlayer cardPlayer,
                                @Context UriInfo uriInfo,
                                @Context HttpHeaders httpHeaders) {

    try {
      cardDealer.addNewCardPlayer(cardGameName, cardPlayer);
      UriBuilder builder = uriInfo.getAbsolutePathBuilder().path(cardPlayer.getName());
      return Response.created(URI.create(configuration.getPublicEndpoint() + builder.build().getPath())).build();
    }
    catch (DuplicateNameException e) {
      return Response.status(Response.Status.CONFLICT)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }

  @DELETE
  @Produces({"application/json"})
  @Path("/{cardGameId}/cardPlayers/{cardPlayerId}")
  public Response deleteCardPlayer(@PathParam("cardGameId") String cardGameName,
                                   @PathParam("cardPlayerId") String cardPlayerName,
                                   @Context UriInfo uriInfo,
                                   @Context HttpHeaders httpHeaders) {
    try {
      cardDealer.deleteCardPlayer(cardGameName, cardPlayerName);
      return Response.noContent().build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }

  @POST
  @Produces({"application/json"})
  @Path("/{cardGameId}/cardPlayers/{cardPlayerId}/cards")
  public Response dealCardToPlayer(@PathParam("cardGameId") String cardGameName,
                                   @PathParam("cardPlayerId") String cardPlayerName,
                                   CardDeal cardDeal,
                                   @Context UriInfo uriInfo,
                                   @Context HttpHeaders httpHeaders) {
    try {
      cardDealer.dealCards(cardGameName, cardPlayerName, cardDeal.getCount());
      return Response.noContent().build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }

  @GET
  @Produces({"application/json"})
  @Path("/{cardGameId}/cardPlayers/{cardPlayerId}/cards")
  public Response getCardsForPlayer(@PathParam("cardGameId") String cardGameName,
                                    @PathParam("cardPlayerId") String cardPlayerName,
                                    @Context UriInfo uriInfo,
                                    @Context HttpHeaders httpHeaders) {
    try {
      CardsList result = cardDealer.getCardsForPlayer(cardGameName, cardPlayerName);
      return Response.status(Response.Status.OK)
                     .entity(result)
                     .type("application/json")
                     .build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }

  @GET
  @Produces({"application/json"})
  @Path("/{cardGameId}/cardPlayers")
  public Response getCardGamePlayersList(@PathParam("cardGameId") String cardGameName,
                                         @Context UriInfo uriInfo,
                                         @Context HttpHeaders httpHeaders) {
    try {
      PlayersList result = cardDealer.getCardGamePlayersList(cardGameName);
      return Response.status(Response.Status.OK)
                     .entity(result)
                     .type("application/json")
                     .build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }

  @GET
  @Produces({"application/json"})
  @Path("/{cardGameId}/cardsDistribution")
  public Response getCardGameDistribution(@PathParam("cardGameId") String cardGameName,
                                          @Context UriInfo uriInfo,
                                          @Context HttpHeaders httpHeaders) {
    try {
      CardsDistribution result = cardDealer.getCardGameDeckDistribution(cardGameName);
      return Response.status(Response.Status.OK)
                     .entity(result)
                     .type("application/json")
                     .build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }

  @GET
  @Produces({"application/json"})
  @Path("/{cardGameId}/cards")
  public Response getRemainingCardsInGame(@PathParam("cardGameId") String cardGameName,
                                          @Context UriInfo uriInfo,
                                          @Context HttpHeaders httpHeaders) {
    try {
      CardsList result = cardDealer.getRemainingCardsInGame(cardGameName);
      return Response.status(Response.Status.OK)
                     .entity(result)
                     .type("application/json")
                     .build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }

  @POST
  @Produces({"application/json"})
  @Path("/{cardGameId}/shuffle")
  public Response shuffleCardGameDeck(@PathParam("cardGameId") String cardGameName,
                                          @Context UriInfo uriInfo,
                                          @Context HttpHeaders httpHeaders) {
    try {
      cardDealer.shuffleCardGameDeck(cardGameName);
      return Response.noContent().build();
    }
    catch (NotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND)
                     .entity(new ApiErrorMessage(e.getMessage()))
                     .type("application/json")
                     .build();
    }
  }
}

