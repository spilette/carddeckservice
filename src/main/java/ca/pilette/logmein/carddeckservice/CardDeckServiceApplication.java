package ca.pilette.logmein.carddeckservice;

import ca.pilette.logmein.carddeckservice.api.resources.CardDeckResource;
import ca.pilette.logmein.carddeckservice.api.resources.CardGameResource;
import ca.pilette.logmein.carddeckservice.config.CardDeckServiceConfiguration;
import ca.pilette.logmein.carddeckservice.dealer.CardDealer;
import ca.pilette.logmein.carddeckservice.dealer.provider.CardDealerInMemory;
import ca.pilette.logmein.carddeckservice.health.BasicHealthCheck;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CardDeckServiceApplication extends Application<CardDeckServiceConfiguration> {

  public static void main(final String[] args) throws Exception {
    new CardDeckServiceApplication().run(args);
  }

  @Override
  public String getName() {
    return "CardDeckService";
  }

  @Override
  public void initialize(final Bootstrap<CardDeckServiceConfiguration> bootstrap) {
    // TODO: application initialization
  }

  @Override
  public void run(final CardDeckServiceConfiguration configuration,
                  final Environment environment) {

    log.info("\n\r**** Welcome to " + configuration.getHouseName() + " ****\n\r");

    log.info("Registering Health Checks");
    environment.healthChecks().register(BasicHealthCheck.getName(), new BasicHealthCheck());

    log.info("Initiating CardDeck Engine");
    CardDealer cardDealer = CardDealerInMemory.getInstance();
    cardDealer.init();

    log.info("Registering REST API Resources");
    environment.jersey().register(new CardGameResource(configuration, cardDealer));
    environment.jersey().register(new CardDeckResource(cardDealer));
  }
}
