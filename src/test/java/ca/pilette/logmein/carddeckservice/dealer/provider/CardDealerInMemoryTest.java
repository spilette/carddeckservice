package ca.pilette.logmein.carddeckservice.dealer.provider;

import ca.pilette.logmein.carddeckservice.dealer.CardDealer;
import ca.pilette.logmein.carddeckservice.dealer.exception.DuplicateNameException;
import ca.pilette.logmein.carddeckservice.dealer.exception.InvalidStateException;
import ca.pilette.logmein.carddeckservice.dealer.exception.NotFoundException;
import ca.pilette.logmein.carddeckservice.dealer.model.DealerCardGame;
import ca.pilette.logmein.carddeckservice.dealer.model.DealerCardPlayer;
import ca.pilette.logmein.carddeckservice.model.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Slf4j
public class CardDealerInMemoryTest {

  private CardDealer dealer;

  @Before
  public void setup() throws DuplicateNameException {
    log.info("Getting the dealer.");
    dealer = CardDealerInMemory.getInstance();
  }

  @Test
  public void createNewCardGame() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
  }

  @Test
  public void deleteCardGame() throws InvalidStateException {

    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    log.info("Calling dealer.deleteCardGame()");
    dealer.deleteCardGame(cardGameName);
    log.info("Calling dealer.deleteCardGame() a 2nd time - expecting NotFoundException");
    boolean notFoundException = false;
    try {
      dealer.deleteCardGame(cardGameName);
    }
    catch (NotFoundException e) {
      notFoundException = true;
    }
    assertTrue(notFoundException);
  }

  @Test
  public void createNewCardDeck() {
    CardDeck cardDeck = new CardDeck();
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardDeck(cardDeck);
  }

  @Test
  public void getCardDeckAvailableCount() {
    log.info("getAvailableCardDeckCount() returns: " + dealer.getAvailableCardDeckCount());
  }

  @Test
  public void addCardDeckToGame() throws InvalidStateException {
    int availableDeckCount = dealer.getAvailableCardDeckCount();
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    for (int i = 0; i < availableDeckCount; i++) {
      log.info("Calling dealer.addCardDeckToGame() for already available deck");
      dealer.addCardDeckToGame(cardGameName);
    }
    log.info("Calling dealer.createNewCardDeck()");
    CardDeck cardDeck = new CardDeck();
    dealer.createNewCardDeck(cardDeck);
    log.info("Calling dealer.addCardDeckToGame()");
    dealer.addCardDeckToGame(cardGameName);
    log.info("Calling dealer.addCardDeckToGame() - no more deck available - expecting InvalidStateException");
    boolean invalidStateException = false;
    try {
      dealer.addCardDeckToGame(cardGameName);
    }
    catch (InvalidStateException e) {
      invalidStateException = true;
    }
    assertTrue(invalidStateException);
  }

  @Test
  public void addNewCardPlayer() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette");
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer);
  }

  @Test
  public void deleteCardPlayer() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette" + testId);
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer);
    log.info("Calling dealer.deleteCardPlayer()");
    dealer.deleteCardPlayer(cardGameName, cardPlayer.getName());
    log.info("Calling dealer.deleteCardPlayer() a 2nd time - expecting NotFoundException");
    boolean notFoundException = false;
    try {
      dealer.deleteCardPlayer(cardGameName, cardPlayer.getName());
    }
    catch (NotFoundException e) {
      notFoundException = true;
    }
    assertTrue(notFoundException);
  }

  @Test
  public void getDealerCardGame() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    log.info("Calling dealer.createNewCardDeck()");
    CardDeck cardDeck = new CardDeck();
    dealer.createNewCardDeck(cardDeck);
    log.info("Calling dealer.addCardDeckToGame()");
    dealer.addCardDeckToGame(cardGameName);
    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette" + testId);
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer);
    DealerCardGame dealerCardGame = dealer.getDealerCardGame(cardGameName);
    log.info(dealerCardGame.toString());
    log.info("dealerCardGame.getCards().size()=" + dealerCardGame.getCards().size());
  }

  @Test
  public void shuffleCardGameDeck() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    log.info("Calling dealer.createNewCardDeck()");
    CardDeck cardDeck = new CardDeck();
    dealer.createNewCardDeck(cardDeck);
    log.info("Calling dealer.addCardDeckToGame()");
    dealer.addCardDeckToGame(cardGameName);
    DealerCardGame dealerCardGame = dealer.getDealerCardGame(cardGameName);
    List<Card> before = dealerCardGame.getCards().stream().map(o -> o).collect(Collectors.toList());
    dealer.shuffleCardGameDeck(cardGameName);
    List<Card> after = dealerCardGame.getCards().stream().map(o -> o).collect(Collectors.toList());
    int distance = 0;
    for (int i = 0; i < before.size(); i++) {
      //log.debug("before: " + before.get(i));
      //log.debug("after: " + after.get(i));
      distance += before.get(i) == after.get(i) ? 0 : 1;
    }
    log.info("Distance between before and after shuffle=" + distance);
    assertNotSame(0, distance);
  }

  @Test
  public void dealCards() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    log.info("Calling dealer.createNewCardDeck()");
    CardDeck cardDeck = new CardDeck();
    dealer.createNewCardDeck(cardDeck);
    log.info("Calling dealer.addCardDeckToGame()");
    dealer.addCardDeckToGame(cardGameName);
    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette" + testId);
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer);
    int deckSize = 52;
    for (int i = 0; i < deckSize; i++) {
      dealer.dealCards(cardGameName, cardPlayer.getName(), 1);
    }
    DealerCardGame dealerCardGame = dealer.getDealerCardGame(cardGameName);
    DealerCardPlayer player = dealerCardGame.getDealerCardPlayers().get(cardPlayer.getName());
    log.info(player.toString());
    assertEquals(deckSize, player.getCards().size());
    dealer.dealCards(cardGameName, cardPlayer.getName(), 1);
    assertEquals(deckSize, player.getCards().size());
  }

  @Test
  public void dealCards2Decks() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    log.info("Calling dealer.createNewCardDeck()");
    CardDeck cardDeck = new CardDeck();
    dealer.createNewCardDeck(cardDeck);
    dealer.createNewCardDeck(cardDeck);
    log.info("Calling dealer.addCardDeckToGame()");
    dealer.addCardDeckToGame(cardGameName);
    dealer.addCardDeckToGame(cardGameName);
    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette" + testId);
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer);
    int deckSize = 52 * 2;
    for (int i = 0; i < deckSize; i++) {
      dealer.dealCards(cardGameName, cardPlayer.getName(), 1);
    }
    DealerCardGame dealerCardGame = dealer.getDealerCardGame(cardGameName);
    DealerCardPlayer player = dealerCardGame.getDealerCardPlayers().get(cardPlayer.getName());
    log.info(player.toString());
    assertEquals(deckSize, player.getCards().size());
    dealer.dealCards(cardGameName, cardPlayer.getName(), 1);
    assertEquals(deckSize, player.getCards().size());
  }

  @Test
  public void getCardsForPlayer() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    log.info("Calling dealer.createNewCardDeck()");
    CardDeck cardDeck = new CardDeck();
    dealer.createNewCardDeck(cardDeck);
    log.info("Calling dealer.addCardDeckToGame()");
    dealer.addCardDeckToGame(cardGameName);
    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette" + testId);
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer);
    int deckSize = 5;
    for (int i = 0; i < deckSize; i++) {
      dealer.dealCards(cardGameName, cardPlayer.getName(), 1);
    }
    CardsList playerCards = dealer.getCardsForPlayer(cardGameName, cardPlayer.getName());
    log.info("player cards: " + playerCards);
    assertEquals(deckSize, playerCards.getCards().size());
  }

  @Test
  public void getCardGamePlayersListSorted() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    CardDeck cardDeck = new CardDeck();
    dealer.createNewCardDeck(cardDeck);
    log.info("Calling dealer.addCardDeckToGame()");
    dealer.addCardDeckToGame(cardGameName);
    CardPlayer cardPlayer1 = new CardPlayer();
    cardPlayer1.setName("spilette" + testId);
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer1);
    CardPlayer cardPlayer2 = new CardPlayer();
    cardPlayer2.setName("joe" + testId);
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer2);
    int deckSize = 5;
    for (int i = 0; i < deckSize; i++) {
      dealer.dealCards(cardGameName, cardPlayer1.getName(), 1);
      dealer.dealCards(cardGameName, cardPlayer2.getName(), 1);
    }
    PlayersList playersList = dealer.getCardGamePlayersList(cardGameName);
    log.info("Players list: " + playersList.getPlayers());
    int lastScore = -1;
    for (DealerCardPlayer player : playersList.getPlayers()) {
      log.info("Player name=" + player.getCardPlayer().getName() + " , and score=" + player.getScore());
      if (lastScore >= 0) {
        assertTrue(lastScore >= player.getScore());
      }
      lastScore = player.getScore();
    }
  }

  @Test
  public void getCardGameDeckDistribution() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    CardDeck cardDeck = new CardDeck();
    dealer.createNewCardDeck(cardDeck);
    log.info("Calling dealer.addCardDeckToGame()");
    dealer.addCardDeckToGame(cardGameName);
    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette" + testId);
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer);
    dealer.dealCards(cardGameName, cardPlayer.getName(), 10);
    CardsDistribution distribution = dealer.getCardGameDeckDistribution(cardGameName);
    log.info("CardGameDeckDistribution=" + distribution.toString() );
    assertEquals( 42,distribution.getHeartsCount() + distribution.getSpadesCount() + distribution.getClubsCount() + distribution.getDiamondsCount());
  }

  @Test
  public void getRemainingCardsInGame() throws InvalidStateException {
    UUID testId = UUID.randomUUID();
    String cardGameName = "LogMeInUnitTesting-" + testId;
    CardGame cardGame = new CardGame();
    cardGame.setName(cardGameName);
    log.info("Calling dealer.createNewCardGame()");
    dealer.createNewCardGame(cardGame);
    CardDeck cardDeck = new CardDeck();
    dealer.createNewCardDeck(cardDeck);
    log.info("Calling dealer.addCardDeckToGame()");
    dealer.addCardDeckToGame(cardGameName);
    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette" + testId);
    log.info("Calling dealer.addNewCardPlayer()");
    dealer.addNewCardPlayer(cardGameName, cardPlayer);
    dealer.dealCards(cardGameName, cardPlayer.getName(), 10);
    CardsList cardsList = dealer.getRemainingCardsInGame(cardGameName);
    log.info("CardGameDeckDistribution=" + cardsList.toString() );
    assertEquals( 42, cardsList.getCards().size());
  }

}