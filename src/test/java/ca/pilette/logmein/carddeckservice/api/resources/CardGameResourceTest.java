package ca.pilette.logmein.carddeckservice.api.resources;

import ca.pilette.logmein.carddeckservice.config.CardDeckServiceConfiguration;
import ca.pilette.logmein.carddeckservice.dealer.model.DealerCardPlayer;
import ca.pilette.logmein.carddeckservice.dealer.provider.CardDealerInMemory;
import ca.pilette.logmein.carddeckservice.model.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Slf4j
public class CardGameResourceTest {

  private CardGameResource cardGameResource;
  private CardDeckResource cardDeckResource;

  private UriInfo mockUriInfo;

  @Before
  public void setup() {

    final CardDeckServiceConfiguration configuration = new CardDeckServiceConfiguration();
    configuration.setHouseName("LogMeIn Unit Testing");
    configuration.setPublicEndpoint("http://localhost:8080");

    final CardDealerInMemory cardDealer = CardDealerInMemory.getInstance();
    cardDealer.init();

    cardGameResource = new CardGameResource(configuration, cardDealer);
    cardDeckResource = new CardDeckResource(cardDealer);

    mockUriInfo = mock(UriInfo.class);
  }

  @Test
  public void createCardGame() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    Response responseDup = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CONFLICT.getStatusCode(), responseDup.getStatus());
    log.info(responseDup.getEntity().toString());
  }

  @Test
  public void deleteCardGame() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingDeleteCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    assertNotNull(response.getLocation());
    String location = response.getLocation().toString();
    log.info("CardGame location is: " + location);
    String id = location.substring(location.lastIndexOf("/") + 1);
    log.info("CardGame id is: " + id);

    Response responseDel = cardGameResource.deleteCardGame(id, null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDel.getStatus());

    Response responseDel404 = cardGameResource.deleteCardGame(id, null);
    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), responseDel404.getStatus());
    log.info(responseDel404.getEntity().toString());
  }

  @Test
  public void addCardDeck() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    CardDeck payloadDeck = new CardDeck();
    Response responseDeck = cardDeckResource.createCardDeck(payloadDeck, resetMockUriInfoForCardDecks(), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeck.getStatus());

    Response responseAddDeck = cardGameResource
      .addCardDeck(payload.getName(), resetMockUriInfoForCardGames(response.getLocation().toASCIIString()), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseAddDeck.getStatus());
  }

  @Test
  public void addCardPlayer() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette");
    Response responsePlayer = cardGameResource
      .addCardPlayer(payload.getName(), cardPlayer, resetMockUriInfoForCardGames(response.getLocation()
                                                                                         .toASCIIString() + "/cardPlayers"), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), responsePlayer.getStatus());
    log.info(responsePlayer.getLocation().toASCIIString());
  }

  @Test
  public void deleteCardPlayer() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette");
    Response responsePlayer = cardGameResource
      .addCardPlayer(payload.getName(), cardPlayer, resetMockUriInfoForCardGames(response.getLocation()
                                                                                         .toASCIIString() + "/cardPlayers"), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), responsePlayer.getStatus());
    String location = responsePlayer.getLocation().toASCIIString();
    log.info(location);

    Response responseDeletePlayer = cardGameResource
      .deleteCardPlayer(payload.getName(), cardPlayer.getName(), resetMockUriInfoForCardGames(response.getLocation()
                                                                                                      .toASCIIString() + "/cardPlayers/" + cardPlayer
        .getName()), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeletePlayer.getStatus());

    Response responseDeletePlayerNF = cardGameResource
      .deleteCardPlayer(payload.getName(), cardPlayer.getName(), resetMockUriInfoForCardGames(response.getLocation()
                                                                                                      .toASCIIString() + "/cardPlayers/" + cardPlayer
        .getName()), null);
    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), responseDeletePlayerNF.getStatus());
  }

  @Test
  public void dealCardToPlayer() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    CardDeck payloadDeck = new CardDeck();
    Response responseDeck = cardDeckResource.createCardDeck(payloadDeck, resetMockUriInfoForCardDecks(), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeck.getStatus());

    Response responseAddDeck = cardGameResource
      .addCardDeck(payload.getName(), resetMockUriInfoForCardGames(response.getLocation().toASCIIString()), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseAddDeck.getStatus());

    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette-" + UUID.randomUUID().toString());
    Response responsePlayer = cardGameResource
      .addCardPlayer(payload.getName(), cardPlayer, resetMockUriInfoForCardGames(response.getLocation()
                                                                                         .toASCIIString() + "/cardPlayers"), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), responsePlayer.getStatus());
    log.info(responsePlayer.getLocation().toASCIIString());

    CardDeal cardDeal = new CardDeal();
    cardDeal.setCount(1);
    cardGameResource
      .dealCardToPlayer(payload.getName(), cardPlayer.getName(), cardDeal, resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cardPlayers/" + cardPlayer.getName() + "/cards"), null);
  }

  @Test
  public void getCardsForPlayer() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    CardDeck payloadDeck = new CardDeck();
    Response responseDeck = cardDeckResource.createCardDeck(payloadDeck, resetMockUriInfoForCardDecks(), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeck.getStatus());

    Response responseAddDeck = cardGameResource
      .addCardDeck(payload.getName(), resetMockUriInfoForCardGames(response.getLocation().toASCIIString()), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseAddDeck.getStatus());

    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette-" + UUID.randomUUID().toString());
    Response responsePlayer = cardGameResource
      .addCardPlayer(payload.getName(), cardPlayer, resetMockUriInfoForCardGames(response.getLocation()
                                                                                         .toASCIIString() + "/cardPlayers"), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), responsePlayer.getStatus());
    log.info(responsePlayer.getLocation().toASCIIString());

    CardDeal cardDeal = new CardDeal();
    cardDeal.setCount(5);
    Response responseDeal = cardGameResource
      .dealCardToPlayer(payload.getName(), cardPlayer.getName(), cardDeal, resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cardPlayers/" + cardPlayer.getName() + "/cards"), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeal.getStatus());

    Response responsePlayerCards = cardGameResource
      .getCardsForPlayer(payload.getName(), cardPlayer.getName(), resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cardPlayers/" + cardPlayer.getName() + "/cards"), null);
    assertEquals(Response.Status.OK.getStatusCode(), responsePlayerCards.getStatus());
    CardsList cards = (CardsList) responsePlayerCards.getEntity();
    log.info(cards.toString());
    assertEquals(5, cards.getCards().size());
  }

  @Test
  public void getCardGamePlayersList() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    CardDeck payloadDeck = new CardDeck();
    Response responseDeck = cardDeckResource.createCardDeck(payloadDeck, resetMockUriInfoForCardDecks(), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeck.getStatus());

    Response responseAddDeck = cardGameResource
      .addCardDeck(payload.getName(), resetMockUriInfoForCardGames(response.getLocation().toASCIIString()), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseAddDeck.getStatus());

    CardPlayer cardPlayer1 = new CardPlayer();
    cardPlayer1.setName("spilette-" + UUID.randomUUID().toString());
    Response responsePlayer1 = cardGameResource
      .addCardPlayer(payload.getName(), cardPlayer1, resetMockUriInfoForCardGames(response.getLocation()
                                                                                          .toASCIIString() + "/cardPlayers"), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), responsePlayer1.getStatus());
    log.info(responsePlayer1.getLocation().toASCIIString());

    CardPlayer cardPlayer2 = new CardPlayer();
    cardPlayer2.setName("joe-" + UUID.randomUUID().toString());
    Response responsePlayer2 = cardGameResource
      .addCardPlayer(payload.getName(), cardPlayer2, resetMockUriInfoForCardGames(response.getLocation()
                                                                                          .toASCIIString() + "/cardPlayers"), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), responsePlayer2.getStatus());
    log.info(responsePlayer2.getLocation().toASCIIString());

    CardDeal cardDeal = new CardDeal();
    cardDeal.setCount(5);

    Response responseDeal1 = cardGameResource
      .dealCardToPlayer(payload.getName(), cardPlayer1.getName(), cardDeal, resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cardPlayers/" + cardPlayer1.getName() + "/cards"), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeal1.getStatus());

    Response responseDeal2 = cardGameResource
      .dealCardToPlayer(payload.getName(), cardPlayer2.getName(), cardDeal, resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cardPlayers/" + cardPlayer2.getName() + "/cards"), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeal2.getStatus());

    Response responsePlayersList = cardGameResource
      .getCardGamePlayersList(payload.getName(), resetMockUriInfoForCardGames("/cardPlayers"), null);

    assertEquals(Response.Status.OK.getStatusCode(), responsePlayersList.getStatus());
    PlayersList players = (PlayersList) responsePlayersList.getEntity();
    log.info(players.toString());
    assertEquals(2, players.getPlayers().size());

    int lastScore = -1;
    for (DealerCardPlayer player : players.getPlayers()) {
      log.info("Player name=" + player.getCardPlayer().getName() + " , and score=" + player.getScore());
      if (lastScore >= 0) {
        assertTrue(lastScore >= player.getScore());
      }
      lastScore = player.getScore();
    }
  }

  @Test
  public void getCardGameDistribution() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    CardDeck payloadDeck = new CardDeck();
    Response responseDeck = cardDeckResource.createCardDeck(payloadDeck, resetMockUriInfoForCardDecks(), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeck.getStatus());

    Response responseAddDeck = cardGameResource
      .addCardDeck(payload.getName(), resetMockUriInfoForCardGames(response.getLocation().toASCIIString()), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseAddDeck.getStatus());

    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette-" + UUID.randomUUID().toString());
    Response responsePlayer = cardGameResource
      .addCardPlayer(payload.getName(), cardPlayer, resetMockUriInfoForCardGames(response.getLocation()
                                                                                         .toASCIIString() + "/cardPlayers"), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), responsePlayer.getStatus());
    log.info(responsePlayer.getLocation().toASCIIString());

    CardDeal cardDeal = new CardDeal();
    cardDeal.setCount(10);
    Response responseDeal = cardGameResource
      .dealCardToPlayer(payload.getName(), cardPlayer.getName(), cardDeal, resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cardPlayers/" + cardPlayer.getName() + "/cards"), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeal.getStatus());

    Response responseDist = cardGameResource
      .getCardGameDistribution(payload.getName(), resetMockUriInfoForCardGames(response.getLocation()
                                                                                       .toASCIIString() + "/cardsDistribution"), null);
    assertEquals(Response.Status.OK.getStatusCode(), responseDist.getStatus());
    CardsDistribution distribution = (CardsDistribution) responseDist.getEntity();
    log.info("CardGameDeckDistribution=" + distribution.toString());
    assertEquals(42, distribution.getHeartsCount() + distribution.getSpadesCount() + distribution
      .getClubsCount() + distribution.getDiamondsCount());
  }

  @Test
  public void getRemainingCardsInGame() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    CardDeck payloadDeck = new CardDeck();
    Response responseDeck = cardDeckResource.createCardDeck(payloadDeck, resetMockUriInfoForCardDecks(), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeck.getStatus());

    Response responseAddDeck = cardGameResource
      .addCardDeck(payload.getName(), resetMockUriInfoForCardGames(response.getLocation().toASCIIString()), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseAddDeck.getStatus());

    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette-" + UUID.randomUUID().toString());
    Response responsePlayer = cardGameResource
      .addCardPlayer(payload.getName(), cardPlayer, resetMockUriInfoForCardGames(response.getLocation()
                                                                                         .toASCIIString() + "/cardPlayers"), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), responsePlayer.getStatus());
    log.info(responsePlayer.getLocation().toASCIIString());

    CardDeal cardDeal = new CardDeal();
    cardDeal.setCount(10);
    Response responseDeal = cardGameResource
      .dealCardToPlayer(payload.getName(), cardPlayer.getName(), cardDeal, resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cardPlayers/" + cardPlayer.getName() + "/cards"), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeal.getStatus());

    Response responseRemainingCards = cardGameResource
      .getRemainingCardsInGame(payload.getName(), resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cards"), null);
    assertEquals(Response.Status.OK.getStatusCode(), responseRemainingCards.getStatus());
    CardsList cards = (CardsList) responseRemainingCards.getEntity();
    log.info(cards.toString());
    assertEquals(42, cards.getCards().size());
  }

  @Test
  public void shuffleCardGameDeck() throws URISyntaxException {
    CardGame payload = new CardGame();
    payload.setName("TestingCreateCardGame-" + UUID.randomUUID().toString());

    Response response = cardGameResource.createCardGame(payload, resetMockUriInfoForCardGames(), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    log.info(response.getLocation().toASCIIString());

    CardDeck payloadDeck = new CardDeck();
    Response responseDeck = cardDeckResource.createCardDeck(payloadDeck, resetMockUriInfoForCardDecks(), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeck.getStatus());

    Response responseAddDeck = cardGameResource
      .addCardDeck(payload.getName(), resetMockUriInfoForCardGames(response.getLocation().toASCIIString()), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseAddDeck.getStatus());

    CardPlayer cardPlayer = new CardPlayer();
    cardPlayer.setName("spilette-" + UUID.randomUUID().toString());
    Response responsePlayer = cardGameResource
      .addCardPlayer(payload.getName(), cardPlayer, resetMockUriInfoForCardGames(response.getLocation()
                                                                                         .toASCIIString() + "/cardPlayers"), null);
    assertEquals(Response.Status.CREATED.getStatusCode(), responsePlayer.getStatus());
    log.info(responsePlayer.getLocation().toASCIIString());

    CardDeal cardDeal = new CardDeal();
    cardDeal.setCount(10);
    Response responseDeal = cardGameResource
      .dealCardToPlayer(payload.getName(), cardPlayer.getName(), cardDeal, resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cardPlayers/" + cardPlayer.getName() + "/cards"), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseDeal.getStatus());

    Response responseRemainingCards1 = cardGameResource
      .getRemainingCardsInGame(payload.getName(), resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cards"), null);
    assertEquals(Response.Status.OK.getStatusCode(), responseRemainingCards1.getStatus());
    CardsList cards1 = (CardsList) responseRemainingCards1.getEntity();
    log.info(cards1.toString());
    assertEquals(42, cards1.getCards().size());

    Response responseShuffle = cardGameResource
      .shuffleCardGameDeck(payload.getName(), resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/shuffle"), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), responseShuffle.getStatus());

    Response responseRemainingCards2 = cardGameResource
      .getRemainingCardsInGame(payload.getName(), resetMockUriInfoForCardGames(response
        .getLocation()
        .toASCIIString() + "/cards"), null);
    assertEquals(Response.Status.OK.getStatusCode(), responseRemainingCards2.getStatus());
    CardsList cards2 = (CardsList) responseRemainingCards2.getEntity();
    log.info(cards2.toString());
    assertEquals(52, cards2.getCards().size());

  }

  private UriInfo resetMockUriInfoForCardGames() throws URISyntaxException {
    return resetMockUriInfoForCardGames("http://localhost:8080/cardGames");
  }

  private UriInfo resetMockUriInfoForCardDecks() throws URISyntaxException {
    return resetMockUriInfoForCardGames("http://localhost:8080/cardDecks");
  }

  private UriInfo resetMockUriInfoForCardGames(String path) throws URISyntaxException {
    when(mockUriInfo.getAbsolutePathBuilder())
      .thenReturn(UriBuilder.fromUri(new URI(path)));
    return mockUriInfo;
  }
}
