package ca.pilette.logmein.carddeckservice.api.resources;

import ca.pilette.logmein.carddeckservice.dealer.provider.CardDealerInMemory;
import ca.pilette.logmein.carddeckservice.model.CardDeck;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Slf4j
public class CardDeckResourceTest {

  private CardDeckResource cardDeckResource;
  private UriInfo mockUriInfo;

  @Before
  public void setup() throws URISyntaxException {

    final CardDealerInMemory cardDealer = CardDealerInMemory.getInstance();
    cardDealer.init();

    cardDeckResource = new CardDeckResource(cardDealer);

    mockUriInfo = mock(UriInfo.class);

  }

  @Test
  public void createCardDeck() throws URISyntaxException {
    CardDeck payload = new CardDeck();

    Response response = cardDeckResource.createCardDeck(payload, resetMockUriInfo(), null);
    assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
  }

  private UriInfo resetMockUriInfo() throws URISyntaxException {
    when(mockUriInfo.getAbsolutePathBuilder())
      .thenReturn(UriBuilder.fromUri(new URI("http://localhost:8080/cardDecks")));
    return mockUriInfo;
  }

}
