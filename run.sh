#!/bin/bash

configFile="$1"
if [[ -z "$configFile" ]]; then
  echo
  echo "ERROR:: config file location must be provided as first argument"
  exit 1
fi

echo
echo "Starting the execution using config file: $configFile"
java -jar target/logmein-carddeck-service-1.0-SNAPSHOT.jar server $configFile