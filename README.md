# CardDeckService

How to start the CardDeckService application
---

1. Run `mvn clean install` to build your application
1. Start application with `./run.sh config/desktop.yaml`

Health Check
---
To see your applications health enter url `http://localhost:8081/healthcheck`

Example
---

1. `curl -i -X DELETE localhost:8080/cardGames/firstGame`
1. `curl -i -X POST localhost:8080/cardGames -H "Content-Type: application/json" -d '{ "name": "firstGame" }'`
1. `curl -i -X POST localhost:8080/cardDecks`
1. `curl -i -X POST localhost:8080/cardGames/firstGame/cardDecks`
1. `curl -i -X POST localhost:8080/cardGames/firstGame/cardDecks`
1. `curl -i -X POST localhost:8080/cardGames/firstGame/cardPlayers -H "Content-Type: application/json" -d '{ "name": "spilette" }'`
1. `curl -i -X POST localhost:8080/cardGames/firstGame/cardPlayers -H "Content-Type: application/json" -d '{ "name": "jpass" }'`
1. `curl -i -X POST localhost:8080/cardGames/firstGame/cardPlayers/spilette/cards -H "Content-Type: application/json" -d '{ "count": 5 }'`
1. `curl -i -X GET localhost:8080/cardGames/firstGame/cardPlayers/spilette/cards`
1. `curl -i -X POST localhost:8080/cardGames/firstGame/cardPlayers/jpass/cards -H "Content-Type: application/json" -d '{ "count": 5 }'`
1. `curl -i -X GET localhost:8080/cardGames/firstGame/cardPlayers/jpass/cards`
1. `curl -i -X GET localhost:8080/cardGames/firstGame/cardPlayers/`
1. `curl -i -X POST localhost:8080/cardGames/firstGame/shuffle`
1. `curl -i -X GET localhost:8080/cardGames/firstGame/cardPlayers/`
1. `curl -i -X POST localhost:8080/cardGames/firstGame/cardPlayers/spilette/cards -H "Content-Type: application/json" -d '{ "count": 5 }'`
1. `curl -i -X POST localhost:8080/cardGames/firstGame/cardPlayers/jpass/cards -H "Content-Type: application/json" -d '{ "count": 5 }'`
1. `curl -i -X GET localhost:8080/cardGames/firstGame/cardPlayers/`
1. `curl -i -X GET localhost:8080/cardGames/firstGame/cardsDistribution`
1. `curl -i -X GET localhost:8080/cardGames/firstGame/cards`
1. `curl -i -X DELETE localhost:8080/cardGames/firstGame/cardPlayers/spilette`
1. `curl -i -X DELETE localhost:8080/cardGames/firstGame/cardPlayers/spilette`
1. `curl -i -X DELETE localhost:8080/cardGames/firstGame`
1. `curl -i -X DELETE localhost:8080/cardGames/firstGame`
